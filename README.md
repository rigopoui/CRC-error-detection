# CRC-error-detection
A small script in python to emulate a cyclic redundancy check to detect errors in a message. Made for educational purposes.

    usage: CRC.py [-h] -w WORDS -l LENGTH -e ERROR -p P
    
    Implementation of cyclic redundancy check for educational purposes
    
    optional arguments:
      -h, --help            show this help message and exit
    
    required named arguments:
      -w WORDS, --words WORDS
                            The number of words to send. Accepted value is a
                            positive integer
      -l LENGTH, --length LENGTH
                            The length of the message to be sent. Accepted value
                            is a positive integer
      -e ERROR, --error_rate ERROR
                            The bit error rate of the channel, values in range
                            [0.0, 1.0]
      -p P                  The number P (n+1 bits) in binary. The number must
                            begin with and end in '1' 


![Example of usage](https://gitlab.com/rigopoui/CRC-error-detection/raw/master/crc-img.png)
