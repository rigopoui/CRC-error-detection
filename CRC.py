"""
   CRC error detection 1.1
  
   Copyright (C) 2018 - Yannis Rigopoulos
 
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

 """
# coding: UTF-8
#CRC υλοποίηση σε python για ανίχνευση λαθών, για εκπαιδευτικούς σκοπούς
#CRC implementation in python for detecting errors, for educational purposes
import random
import argparse

#Δημιουργεί ένα τυχαίο μήνυμα M, μήκους M_length bits
#Creates a random message M, of length M_length bits
def message_generate(M_length): 
	M = []
	M.append('1')
	for i in range(1, M_length):
		chance = random.random()
		if chance >= 0.5:
			M.append('1')
		else:
			M.append('0')
	return M

#Κωδικοποιεί το μήνυμα Μ με τον αριθμό CRC που δώθηκε
#Encodes the message M with the given CRC number		
def crc_encode(M, P): 
	j = 0
	flag = True
	T = list(M)
	for i in range(0, len(P)-1):
		M.append('0')
	while flag == True:
		if j+len(P) > len(M):
			F = M[len(M)-len(P)+1:]
			flag = False
		elif M[j]=='1':
			for k in range(0, len(P)):
				M[k+j] = str(int(M[k+j]) ^ int(P[k]))
			j+=1
		else:
			j+=1
	for i in range(0, len(F)):
		T.append(F[i])
	return T

#Ελέγχει αν υπάρχουν σφάλματα στο μήνυμα, επιστρέφει True εαν ανιχνεύσει σφάλμα
#Checks for errors in the message, returns True if errors are found
def crc_error_check(T, P): 
	j = 0
	flag = True
	while flag == True:
		if j+len(P) > len(T):
			flag = False
		elif T[j]=='1':
			for k in range(0, len(P)):
				T[k+j] = str(int(T[k+j]) ^ int(P[k]))
			j+=1
		else:
			j+=1
	if all(x == '0' for x in T):
		return False
	else:
		return True

#Τυχαία ανάλογα με το error_rate που έχει δοθεί αλλάζει bits του σήματος
#Επιστρέφει True εαν εισάγει τουλάχιστον 1 σφάλμα στο μήνυμα
#Randomly, depending on the given error_rate changes bits of the message
#Returns True if at least 1 error has been inserted to the message
def error_generate(T, width):
	flag = False
	if width != 0:
		chance = random.randrange(1, width + 1)
		for i in range(0, len(T)):
			if random.randrange(1, width + 1) == chance:
				flag = True
				if T[i] == '1':
					T[i] = '0'
				else:
					T[i] = '1'
	return flag


#Ελέγχει αν ένας ακέραιος είναι θετικός
#Checks if an integer is positive
def positive_int(num):
	num = int(num)
	if num > 0:
		return num
	else:
		raise argparse.ArgumentTypeError("%s is an invalid positive int value" % num)

#Ελέγχει αν ένας float είναι μεταξύ 0 και 1
#Checks if a float is between 0 and 1
def restricted_float(num):
    num = float(num)
    if num < 0.0 or num > 1.0:
        raise argparse.ArgumentTypeError("%r not in range [0.0, 1.0]"%(num,))
    return num

#Ελέγχει αν ο δυαδικός αριθμός P είναι συμβατός για crc
#Checks if the binary number P is valid for crc
def valid_crc_num(num):
	if (num[0] != '1' or num[-1] != '1') or not all(x == '0' or x == '1' for x in num):
		raise argparse.ArgumentTypeError("%s is an invalid binary number. The binary number P must begin and end with 1." % num)
	return num

parser = argparse.ArgumentParser(description='Implementation of cyclic redundancy check for educational purposes')
requiredNamed = parser.add_argument_group('required named arguments')
requiredNamed.add_argument('-w', '--words',  dest='words', type=positive_int, required=True,
					help='The number of words to send. Accepted value is a positive integer')
requiredNamed.add_argument('-l', '--length', dest='length', type=positive_int, required=True,
					help='The length of the message to be sent. Accepted value is a positive integer')
requiredNamed.add_argument('-e', '--error_rate', dest='error', type=restricted_float, required=True,
					help='The bit error rate of the channel, values in range [0.0, 1.0]')
requiredNamed.add_argument('-p', dest='P', type=valid_crc_num, required=True,
					help="The number P (n+1 bits) in binary. The number must begin and end in '1'")

#parsing the arguments
args = parser.parse_args()

#main part of program
words_to_send = args.words
M_length = args.length
E = args.error
P = list(args.P)
error_count = 0
error_detected_count = 0
if E != 0: 				
	width = int(1/E)
else:
	width = 0
for i in range(0, words_to_send):
	M = message_generate(M_length)
	T = crc_encode(M, P)
	error = error_generate(T, width)
	error_detected = crc_error_check(T, P)
	if error == True:
		error_count+= 1
	if error_detected == True:
		error_detected_count+= 1
error_percentage = 100.0*float(error_count)/words_to_send
error_detected_percentage = 100.0*float(error_detected_count)/words_to_send
error_not_detected_percentage = 100*float(error_count - error_detected_count)/words_to_send
print("Number of erroneous words: %d (%.2f%%)" % (error_count, error_percentage))
print("Number of erroneous words detected by CRC: %d (%.2f%%)" % (error_detected_count, error_detected_percentage))
print("Number of erroneous words not detected by CRC: %d (%.2f%%)" % (error_count - error_detected_count, error_not_detected_percentage))